package main;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import com.sun.org.apache.xml.internal.security.utils.Base64;

public class Cam {
    private static int bufAvailable;
    private static int bufRead;
    Cam.Buffer vbuf;
    private JLabel imageLable;
    private boolean running;

    public Cam(JFrame frame) {
        this.vbuf = new Cam.Buffer(30, 204800);
        this.running = true;

        this.imageLable = new JLabel();
        frame.add(this.imageLable);
    }

    public class Buffer {
        public static final int BF_FREE = 0;
        public static final int BF_IN_USE = 1;
        public static final int BF_WRITE_OK = 2;
        int maxBufSlot = 30;
        int maxBufSize = 204800;
        public byte[][] buf;
        public int[] state;
        public int[] len;
        public int totalframes;
        public int cnt;
        public int sec;
        public int usec;
        public int idx;

        public void clearBuf() {
            for (int i = 0; i < this.maxBufSlot; i++) {
                this.state[i] = 0;
                this.len[i] = 0;
                this.idx = 0;
            }
        }

        public Buffer() {
            this.buf = new byte[this.maxBufSlot][this.maxBufSize];
            clearBuf();
        }

        public Buffer(int paramInt1, int paramInt2) {
            this.maxBufSlot = paramInt1;
            this.maxBufSize = paramInt2;
            this.state = new int[paramInt1];
            this.len = new int[paramInt1];
            this.buf = new byte[this.maxBufSlot][this.maxBufSize];
            clearBuf();
        }
    }

    public void ShowThread() {
        boolean bool = true;
        int i = 0;
        while (this.running == true) {
            if (this.vbuf.state[this.vbuf.idx] != 2) {
                nonsafeSleep(1L);
                i++;
            } else {
                i = 0;
                this.vbuf.state[this.vbuf.idx] = 1;

                bool = checkJPG(this.vbuf.buf[this.vbuf.idx], this.vbuf.len[this.vbuf.idx]);
                if (bool) {
                    try {
                        BufferedImage img = ImageIO.read(new ByteArrayInputStream(this.vbuf.buf[this.vbuf.idx]));
                        ImageIcon icon = new ImageIcon(img);
                        this.imageLable.setIcon(icon);

                        this.vbuf.state[this.vbuf.idx] = 0;
                        this.vbuf.idx = ((this.vbuf.idx + 1) % this.vbuf.maxBufSlot);
                    } catch (IOException e) {
                        System.out.println("faild image");
                    }

                } else {
                    nonsafeSleep(1L);
                    this.vbuf.state[this.vbuf.idx] = 0;
                    this.vbuf.idx = ((this.vbuf.idx + 1) % this.vbuf.maxBufSlot);
                }
            }
        }
    }

    public void GetThread() {
        HttpURLConnection localHttpURLConnection = null;
        Random localRandom = new Random(System.currentTimeMillis());
        while (this.running == true)
        {
            int i = 0;
            int j = 0;

            bufAvailable = 0;
            bufRead = 0;
            try {
                URL localURL = new URL("http", "1.2.3.4", 80, "/cgi/mjpg/mjpeg.cgi");

                localHttpURLConnection = (HttpURLConnection) localURL.openConnection();
                String authCode = Base64.encode("admin:admin".getBytes());
                localHttpURLConnection.setRequestProperty("Authorization", "Basic " + authCode);

                localHttpURLConnection.connect();
                BufferedInputStream localBufferedInputStream = new BufferedInputStream(localHttpURLConnection.getInputStream());
                if (localHttpURLConnection.getResponseCode() != 200) {
                    System.out.println("response  = " + localHttpURLConnection.getResponseCode());
                    continue;
                }
                String str1 = localHttpURLConnection.getContentType();

                String str2 = parseBoundary(str1);
                if (str2.length() == 0) {
                    System.out.println("no boundary, exit");
                    continue;
                }
                str2 = "--" + str2;

                seekToPattern(localBufferedInputStream, str2);
                seekToPattern(localBufferedInputStream, "\r\n\r\n");
                int k = 0;
                while (this.running == true) {
                    while (this.vbuf.state[j] == 1) {
                        j = (j + 1) % this.vbuf.maxBufSlot;
                    }
                    k = 0;

                    this.vbuf.state[j] = 1;
                    i = getOneBoundary(localBufferedInputStream, str2, this.vbuf.buf[j]);
                    if (i <= 0) {
                        throw new IOException("connection timeout");
                    }
                    this.vbuf.state[j] = 2;

                    this.vbuf.len[j] = i;
                    j = (j + 1) % this.vbuf.maxBufSlot;
                }
            } catch (Exception localException) {
                System.out.println("fail show image");
                localHttpURLConnection.disconnect();
                localHttpURLConnection = null;
                this.vbuf.clearBuf();
                j = 0;
                nonsafeSleep(3000 + localRandom.nextInt() % 2000);
            }
        }
        System.err.println("Quit");
        localHttpURLConnection.disconnect();
        this.vbuf.clearBuf();
    }

    private boolean checkJPG(byte[] paramArrayOfByte, int paramInt) {
        boolean bool = false;
        if (paramInt < 1) {
            return false;
        }
        if ((paramArrayOfByte[0] == -1) && (paramArrayOfByte[1] == -40)) {
            bool = true;
        } else {
            System.err.println("error JPG header");
            return false;
        }
        if ((paramArrayOfByte[(paramInt - 1)] == -1)
                && (paramArrayOfByte[paramInt] == -39)) {
            bool = true;
        } else {
            System.err.println("error JPG tailer");
            return false;
        }
        return bool;
    }

    private static void seekToPattern(BufferedInputStream paramBufferedInputStream, String paramString)
            throws IOException {
        int i = readBufIn(paramBufferedInputStream);
        int j = 0;
        int k = 0;
        while (i != -1) {
            if (paramString.charAt(j) == i) {
                j++;
            } else {
                j = 0;
            }
            k++;
            if (j == paramString.length()) {
                return;
            }
            i = readBufIn(paramBufferedInputStream);
        }
        throw new IOException("Boundary not complete");
    }

    private static int readBufIn(BufferedInputStream paramBufferedInputStream)
            throws IOException {
        int i = -1;
        if (bufRead == bufAvailable) {
            checkBufIn(paramBufferedInputStream);
        }
        i = paramBufferedInputStream.read();
        bufRead += 1;
        return i;
    }

    private static void checkBufIn(BufferedInputStream paramBufferedInputStream)
            throws IOException {
        long l1 = System.currentTimeMillis();
        long l2 = System.currentTimeMillis();
        long l3 = 0L;
        while ((bufAvailable = paramBufferedInputStream.available()) == 0) {
            nonsafeSleep(1L);
            l1 = System.currentTimeMillis();
            l3 = l1 - l2;
            if (l3 > 10000L) {
                System.out.println("timeout");
                throw new IOException("timeout");
            }
        }
        l2 = l1;
        bufRead = 0;
    }

    private static String parseBoundary(String paramString) {
        if (paramString.indexOf("multipart/mixed") < 0) {
            return new String("");
        }
        int i = paramString.indexOf("boundary=");
        if (i < 0) {
            return new String("");
        }
        return paramString.substring(i + "boundary=".length());
    }

    private static void nonsafeSleep(long paramLong) {
        try {
            Thread.sleep(paramLong);
        } catch (InterruptedException localInterruptedException) {
            System.err.println("interrupted error");
        }
    }

    private static int getOneBoundary(
            BufferedInputStream paramBufferedInputStream, String paramString,
            byte[] paramArrayOfByte) throws IOException {
        int i = 0;
        int j = 0;
        int k = 0;
        int m = 0;
        int n = 0;
        int i1 = 0;

        seekToPattern(paramBufferedInputStream, "\r\n\r\n");
        m = readBufIn(paramBufferedInputStream);
        while (i1 < 28) {
            m = readBufIn(paramBufferedInputStream);
            if (m == -1) {
                return -1;
            }
            i1++;
        }
        while (m != -1) {
            paramArrayOfByte[i] = ((byte) m);
            if (i >= 204799) {
                System.err.println("buffer size overflow");
                i = 0;
            } else {
                if (paramString.charAt(k) == m) {
                    k++;
                    n++;
                } else {
                    k = 0;
                    n = 0;
                }
                j++;
                if (k == paramString.length()) {
                    return i - paramString.length();
                }
                i++;
                m = readBufIn(paramBufferedInputStream);
            }
        }
        if (m == -1) {
            return -1;
        }
        return i;
    }
}

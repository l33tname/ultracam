package main;

import java.awt.EventQueue;
import javax.swing.JFrame;
import main.Cam;

public class Main {

    private JFrame frame;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Main window = new Main();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public Main() {
        initialize();

        final Cam u = new Cam(frame);

        new Thread(new Runnable() {
            public void run() {
                u.GetThread();
            }
        }).start();

        new Thread(new Runnable() {
            public void run() {
                    u.ShowThread();
                }
        }).start();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 450, 300);
        frame.setSize(640, 480);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}
